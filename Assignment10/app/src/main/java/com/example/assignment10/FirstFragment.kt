package com.example.assignment10

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.example.assignment10.databinding.FragmentFirstBinding
import com.google.android.material.tabs.TabLayoutMediator

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FirstFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FirstFragment : Fragment() {

    lateinit var viewPager: ViewPager2
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private lateinit var dataset: MutableList<Item>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        viewPager = binding.pager
        dataset = setData()
        viewPager.adapter = Adapter(requireContext(),dataset)
        viewPager.setPageTransformer(ZoomOutPageTransformer())
        return binding.root
    }
    //for tablayout
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val tabLayout = binding.tabLayout
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = "PICTURE ${(position + 1)}"
        }.attach()
    }


  private  fun setData(): MutableList<Item> {
      return mutableListOf<Item>(
          Item(R.string.title1, R.string.description, R.drawable.image1),
          Item(R.string.title2, R.string.description, R.drawable.image2),
          Item(R.string.title3, R.string.description, R.drawable.image3),
          Item(R.string.title4, R.string.description, R.drawable.image4),
          Item(R.string.title5, R.string.description, R.drawable.image5)
      )
  }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}