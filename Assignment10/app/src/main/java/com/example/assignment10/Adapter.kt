package com.example.assignment10

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment10.databinding.FragmentImageBinding

class Adapter(val context: Context, private val imgGallery: MutableList<Item>): RecyclerView.Adapter<Adapter.ImageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ImageViewHolder {
        return ImageViewHolder(FragmentImageBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
       val model = imgGallery[position]
        val itemPosition = position + 1
        val res = context.resources.getString(model.description)
            holder.image.setImageResource(model.imgSource)
            holder.descr.text = res
            holder.title.text = context.resources.getString(model.title)


        /** This 2 line is for show item position and list size
         * But it's shown by tablayout too
         * here is provided both way
         */
            holder.itemPos.text = context.resources.getString(R.string.position).plus(itemPosition.toString())
            holder.size.text = context.resources.getString(R.string.size).plus(imgGallery.size.toString())
    }

    override fun getItemCount() = imgGallery.size
    inner class ImageViewHolder(private val binding: FragmentImageBinding) : RecyclerView.ViewHolder(binding.root) {

            val image: ImageView = binding.image
            val descr: TextView = binding.description
            val title:TextView = binding.title
            val itemPos:TextView = binding.positionTv
            val size: TextView = binding.gallerySize

    }

}