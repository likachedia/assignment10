package com.example.assignment10

import androidx.annotation.StringRes

data class Item(@StringRes var title: Int, @StringRes var description: Int, var imgSource: Int) {

}

